/**
 * 1. 通过键值形式定义语言
 * 2. 通过useI18n().t() 展示语言
 */
export default {
  roleAuthority: '角色权限',
  dataAuthority: '数据权限',
  saasRoleAuthority: 'SAAS角色权限',
  threeMemberManagement: '三员管理',
  left: {
    role: '角色',
    department: '组织',
    organization: '机构',
    permissionGroup: '权限组',
    developer: '开发者',
  },
  default: {
    account: '账户配置',
    permission: ' 权限配置',
    title: '北京凯特伟业提供技术支持 电话：400-0999-235',
  },
  account: {
    search: '搜索',
    remove: '移除',
    look: '查看权限',
  },
};

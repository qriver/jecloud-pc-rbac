import { h } from 'vue';

import DataAuthority from '@/views/data-authority/open-dialog/index.vue';

import { Modal } from '@jecloud/ui';

export function showDataAuthorityWin({ funcCode, funcId }, fn) {
  let parentModal = {};
  parentModal = Modal.window({
    title: '数据权限',
    width: '1250px',
    minWidth: '1250px',
    resize: false,
    content() {
      return h(DataAuthority, { funcCode, funcId, parentModal });
    },
    //关闭方法
    // eslint-disable-next-line no-unused-vars
    onClose(model) {
      fn();
    },
  });
}

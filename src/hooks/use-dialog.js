import { Modal } from '@jecloud/ui';
import VueEvent from '@/helper/model/event';

export const useDialog = ({ toReset, save, isDicOrRole = false }) => {
  const dialogEvent = async () => {
    Modal.dialog({
      content: '数据有改动，未保存，确定离开吗？',
      status: 'warning',
      icon: 'fal fa-question-circle',
      buttons: [
        {
          text: '确认离开',
          type: 'primary',
          closable: false,
          handler: ({ button, $modal }) => {
            button.loading = true;
            $modal.close();
            toReset();
            VueEvent.emit('isChange', true);
          },
        },
        {
          text: '保存离开',
          type: 'primary',
          closable: false,
          handler: ({ button, $modal }) => {
            button.disabled = true;
            button.disabled = false;
            $modal.close();
            save();
            if (isDicOrRole) {
              toReset();
            }
            setTimeout(() => {
              VueEvent.emit('isChange', true);
            }, 300);
          },
        },
        {
          text: '取消',
          handler: ({ button, $modal }) => {
            button.disabled = true;
            button.disabled = false;
            $modal.close();
            setTimeout(() => {
              VueEvent.emit('isChange', false);
            }, 0);
          },
        },
      ],
    });
  };
  return dialogEvent();
};

/**
 * API_URL命名规则：API_模块_方法
 */
// 加载角色树
export const API_RBAC_LOADROLETREE = '/je/rbac/cloud/grant/role/loadRoleTree';

// 添加分类
export const API_RBAC_ADDFOLDER = '/je/rbac/cloud/grant/role/addFolder';

// 更新分类
export const API_RBAC_UPDATEFOLDER = '/je/rbac/cloud/grant/role/updateFolder';

// 删除分类
export const API_RBAC_REMOVEFOLDER = '/je/rbac/cloud/grant/role/removeFolder';

// 添加角色
export const API_RBAC_ADDROLE = '/je/rbac/cloud/grant/role/addRole';

// 更新角色
export const API_RBAC_UPDATEROLE = '/je/rbac/cloud/grant/role/updateRole';

// 删除角色
export const API_RBAC_REMOVEROLE = '/je/rbac/cloud/grant/role/removeRole';

// 移动角色
export const API_RBAC_MOVEROLE = '/je/rbac/cloud/grant/role/moveRole';

// 加载顶级菜单
export const API_RBAC_LOADTOPMENUS = '/je/rbac/cloud/grant/loadTopMenus';

// 导入功能授权
export const API_RBAC_IMPORTFUNCS = '/je/rbac/cloud/grant/importFuncs';

// 加载功能授权
export const API_RBAC_LOADIMPORTFUNCS = '/je/rbac/cloud/grant/loadImportFuncs';

// 删除功能授权
export const API_RBAC_REMOVEFUNCS = '/je/rbac/cloud/grant/removeFuncs';

// 查询全部机构
export const API_RBAC_LOADORG = '/je/rbac/cloud/grant/org/loadOrgs';

// 加载账户
export const API_RBAC_LOADACCOUNT = '/je/rbac/cloud/grant/loadAccount';

// 加载账户权限
export const API_RBAC_LOADACCOUNTPERMISSIONTREE = '/je/rbac/cloud/grant/loadAccountPermissionTree';

// 按人员导入账号
export const API_RBAC_IMPORTBYDEPARTUSERS =
  '/je/rbac/cloud/grant/role/importAccountUserByDepartmentUsers';

// 按部门导入
export const API_RBAC_IMPORTBYDEPARTMENT =
  '/je/rbac/cloud/grant/role/importAccountUserByDepartment';

// 自主控制角色权限
export const API_RBAC_SELFCONTROLPERMROLE = '/je/rbac/cloud/grant/selfControlPermByRole';

// 自主控制部门权限
export const API_RBAC_SELFCONTROLPERMDEPARTMENT =
  '/je/rbac/cloud/grant/selfControlPermByDepartment';

// 取消角色自主控制
export const API_RBAC_CANCELSELFCONTROLPERMROLE =
  '/je/rbac/cloud/grant/cancelSelfControlPermByRole';

// 取消部门自主控制
export const API_RBAC_CANCELSELFCONTROLPERMDEPARTMENT =
  '/je/rbac/cloud/grant/cancelSelfControlPermByDepartment';

// 按机构导入
export const API_RBAC_IMPORTBYORG = '/je/rbac/cloud/grant/role/importAccountUserByOrg';

// 移除账号角色
export const API_RBAC_REMOVEACCOUNT = '/je/rbac/cloud/grant/role/removeAccountRole';

// 加载权限树
export const API_RBAC_LOADPERMISSIONTREE = '/je/rbac/cloud/grant/loadPermissionTree';

// 加载功能授权树
export const API_RBAC_LOADFUNCPERMISSIONTREE = '/je/rbac/cloud/grant/loadFuncPermissionTree';

// 获取部门树
export const API_RBAC_LOADDEPARTMENTTREE = '/je/rbac/cloud/grant/department/loadDepartmentTree';

// 加载公司部门人员树
export const API_RBAC_LOADDEPARTMENTUSERTREE =
  '/je/rbac/cloud/grant/department/loadtDepartmentUserTree';

// 加载部门人员
export const API_RBAC_LOADROLEUSER = '/je/rbac/cloud/grant/department/loadRoleUser';

// 保存角色菜单授权
export const API_RBAC_SAVEROLEMENUPERMISSION = '/je/rbac/cloud/grant/role/saveRoleMenuPermission';

// 保存部门菜单授权
export const API_RBAC_SAVEDEPMENUPERMISSION =
  '/je/rbac/cloud/grant/department/saveDeptMenuPermission';

// 保存机构菜单授权
export const API_RBAC_SAVEORGMENUPERMISSION = '/je/rbac/cloud/grant/org/saveOrgMenuPermission';

// 保存权限组菜单授权
export const API_RBAC_SAVEPERMGROUPMENUPERMISSION =
  '/je/rbac/cloud/grant/permGroup/savePermGroupMenuPermission';

// 保存开发组菜单授权
export const API_RBAC_SAVEDEVELOPMENUPERMISSION =
  '/je/rbac/cloud/grant/develop/saveDevelopMenuPermission';

// 保存角色功能授权
export const API_RBAC_SAVEROLEFUNCPERMISSION = '/je/rbac/cloud/grant/role/saveRoleFuncPermission';

// 保存部门功能授权
export const API_RBAC_SAVEDEPFUNCPERMISSION =
  '/je/rbac/cloud/grant/department/saveDeptFuncPermission';

// 保存机构功能授权
export const API_RBAC_SAVEORGFUNCPERMISSION =
  '/je/rbac/cloud/grant/permGroup/saveOrgMenuPermission';

// 保存权限组功能授权
export const API_RBAC_SAVEPERMGROUPFUNCPERMISSION =
  '/je/rbac/cloud/grant/permGroup/savePermGroupFuncPermission';

// 保存开发组功能授权
export const API_RBAC_SAVEDEVELOPFUNCPERMISSION =
  '/je/rbac/cloud/grant/develop/saveDevelopFuncPermission';

// 移除部门人员账号
export const API_RBAC_REMOVEDEPTUSER = '/je/rbac/cloud/grant/department/removeDeptUser';

// 移除所有部门人员账号
export const API_RBAC_REMOVEALLDEPTUSER = '/je/rbac/cloud/grant/department/removeAllDeptUser';

// 加载权限组
export const API_RBAC_LOADPERMGROUPS = '/je/rbac/cloud/grant/permGroup/loadPermGroups';

// 创建权限组
export const API_RBAC_SAVEPERMGROUPS = '/je/rbac/cloud/grant/permGroup/savePermGroup';

// 更新权限组
export const API_RBAC_UPDATEPERMGROUPS = '/je/rbac/cloud/grant/permGroup/updatePermGroup';

// 移除权限组
export const API_RBAC_REMOVEPERMGROUPS = '/je/rbac/cloud/grant/permGroup/removePermGroup';

// 移动权限组
export const API_RBAC_MOVEGROUPS = '/je/rbac/cloud/grant/permGroup/moveGroup';

// 开发组-加载角色树
export const API_RBAC_DEVELOPLOADROLETREE = '/je/rbac/cloud/grant/develop/loadRoleTree';

// 添加分类
export const API_RBAC_ADDDEVFOLDER = '/je/rbac/cloud/grant/develop/addFolder';

// 开发组-更新文件夹
export const API_RBAC_UPDATEDEVFOLDER = '/je/rbac/cloud/grant/develop/updateFolder';

// 开发组-删除文件夹
export const API_RBAC_REMOVEDEVFOLDER = '/je/rbac/cloud/grant/develop/removeFolder';

// 开发组-添加角色
export const API_RBAC_ADDDEVROLE = '/je/rbac/cloud/grant/develop/addRole';

// 开发组-更新角色
export const API_RBAC_UPDATEDEVROLE = '/je/rbac/cloud/grant/develop/updateRole';

// 开发组- 删除角色
export const API_RBAC_REMOVEDEVROLE = '/je/rbac/cloud/grant/develop/removeRole';

// 开发组-移动角色
export const API_RBAC_MOVEDEVROLE = '/je/rbac/cloud/grant/develop/moveRole';

// 是否显示开发者
export const API_RBAC_LOADDEVELOPTAB = '/je/rbac/cloud/grant/loadDevelopTab';

// 开发者导入账号.
export const API_RBAC_IMPOTRACCOUNTDDEPTBYDDEPTACCOUNT =
  '/je/rbac/cloud/grant/role/importDevelopAccountDept';

// 开发者删除账号
export const REMOVEDEVELOPACCOUNTDEPTROLE =
  '/je/rbac/cloud/grant/role/removeDevelopAccountDeptRole';

// SAAS角色权限 查看规划产品角色树
export const API_SAAS_RBAC_FINDPRODUCTROLETREE = '/je/saas/productRole/findProductRoleTree';

// SAAS角色权限 查看规划产品顶级菜单
export const API_SAAS_RBAC_LOADTOPMENUS = '/je/saas/productRole/loadTopMenus';

// SAAS角色权限 查看规划产品顶级功能
export const API_SAAS_RBAC_LOADTOPFUNCS = '/je/saas/productRole/loadTopFuncs';

// SAAS角色权限 查看规划产品菜单树
export const API_SAAS_RBAC_FINDPRODUCTMENUTREE = '/je/saas/productRole/findProductMenuTree';

// SAAS角色权限 查看规划产品功能树
export const API_SAAS_RBAC_FINDPRODUCTFUNCTREE = '/je/saas/productRole/findProductFuncTree';

// SAAS角色权限 规划产品授权
export const API_SAAS_RBAC_GRANT = '/je/saas/productRole/grant';

// 加载三员角色树
export const API_THEREEMEMBERMANAGEMENT_LOAD_ROLE_TREE =
  '/je/rbac/threemember/authorize/loadTreeMemberRoleTree';

// 加载三员角色账号
export const API_THEREEMEMBERMANAGEMENT_LOAD_ACCOUNT = '/je/rbac/threemember/authorize/loadAccount';

// 加载三员顶部菜单
export const API_THEREEMEMBERMANAGEMENT_LOAD_TOP_MENUS =
  '/je/rbac/threemember/authorize/loadTopMenus';

// 加载三员权限树
export const API_THEREEMEMBERMANAGEMENT_LOAD_PERMISSION_TREE =
  '/je/rbac/threemember/authorize/loadPermissionTree';

// 三员保存菜单权限
export const API_THEREEMEMBERMANAGEMENT_SAVE_POLE_MENU_PERMISSION =
  '/je/rbac/threemember/authorize/saveRoleMenuPermission';

/**
 * 用于编写api方法
 * api url 统一在urls.js中声明导出使用，与方法分开
 */
import { ajax } from '@jecloud/utils';
import {
  API_RBAC_ADDFOLDER,
  API_RBAC_ADDROLE,
  API_RBAC_IMPORTBYDEPARTUSERS,
  API_RBAC_IMPORTFUNCS,
  API_RBAC_LOADACCOUNT,
  API_RBAC_LOADIMPORTFUNCS,
  API_RBAC_LOADORG,
  API_RBAC_LOADROLETREE,
  API_RBAC_LOADTOPMENUS,
  API_RBAC_MOVEROLE,
  API_RBAC_REMOVEFOLDER,
  API_RBAC_REMOVEFUNCS,
  API_RBAC_REMOVEROLE,
  API_RBAC_UPDATEFOLDER,
  API_RBAC_UPDATEROLE,
  API_RBAC_REMOVEACCOUNT,
  API_RBAC_LOADPERMISSIONTREE,
  API_RBAC_LOADDEPARTMENTTREE,
  API_RBAC_IMPORTBYORG,
  API_RBAC_IMPORTBYDEPARTMENT,
  API_RBAC_LOADDEPARTMENTUSERTREE,
  API_RBAC_LOADROLEUSER,
  API_RBAC_LOADFUNCPERMISSIONTREE,
  API_RBAC_SAVEROLEMENUPERMISSION,
  API_RBAC_SAVEDEPMENUPERMISSION,
  API_RBAC_SAVEORGMENUPERMISSION,
  API_RBAC_SAVEROLEFUNCPERMISSION,
  API_RBAC_SAVEDEPFUNCPERMISSION,
  API_RBAC_SAVEORGFUNCPERMISSION,
  API_RBAC_REMOVEDEPTUSER,
  API_RBAC_REMOVEALLDEPTUSER,
  API_RBAC_LOADACCOUNTPERMISSIONTREE,
  API_RBAC_LOADPERMGROUPS,
  API_RBAC_SAVEPERMGROUPS,
  API_RBAC_REMOVEPERMGROUPS,
  API_RBAC_UPDATEPERMGROUPS,
  API_RBAC_MOVEGROUPS,
  API_RBAC_SAVEPERMGROUPMENUPERMISSION,
  API_RBAC_SAVEPERMGROUPFUNCPERMISSION,
  API_RBAC_SAVEDEVELOPMENUPERMISSION,
  API_RBAC_SAVEDEVELOPFUNCPERMISSION,
  API_RBAC_DEVELOPLOADROLETREE,
  API_RBAC_ADDDEVFOLDER,
  API_RBAC_UPDATEDEVFOLDER,
  API_RBAC_REMOVEDEVFOLDER,
  API_RBAC_MOVEDEVROLE,
  API_RBAC_REMOVEDEVROLE,
  API_RBAC_UPDATEDEVROLE,
  API_RBAC_ADDDEVROLE,
  API_RBAC_SELFCONTROLPERMROLE,
  API_RBAC_SELFCONTROLPERMDEPARTMENT,
  API_RBAC_CANCELSELFCONTROLPERMROLE,
  API_RBAC_CANCELSELFCONTROLPERMDEPARTMENT,
  API_RBAC_LOADDEVELOPTAB,
  REMOVEDEVELOPACCOUNTDEPTROLE,
  API_RBAC_IMPOTRACCOUNTDDEPTBYDDEPTACCOUNT,
  API_SAAS_RBAC_FINDPRODUCTROLETREE,
  API_SAAS_RBAC_LOADTOPMENUS,
  API_SAAS_RBAC_LOADTOPFUNCS,
  API_SAAS_RBAC_FINDPRODUCTMENUTREE,
  API_SAAS_RBAC_FINDPRODUCTFUNCTREE,
  API_SAAS_RBAC_GRANT,
  API_THEREEMEMBERMANAGEMENT_LOAD_ROLE_TREE,
  API_THEREEMEMBERMANAGEMENT_LOAD_ACCOUNT,
  API_THEREEMEMBERMANAGEMENT_LOAD_TOP_MENUS,
  API_THEREEMEMBERMANAGEMENT_LOAD_PERMISSION_TREE,
  API_THEREEMEMBERMANAGEMENT_SAVE_POLE_MENU_PERMISSION,
} from './urls';

/**
 * 加载角色树
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadRoleTreeApi(params, type) {
  let url = API_RBAC_LOADROLETREE;
  // 三员管理判断
  if (type == '/threeMemberManagement') {
    url = API_THEREEMEMBERMANAGEMENT_LOAD_ROLE_TREE;
  }
  return ajax({ method: 'GET', url, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 添加分类
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addFolderApi(params) {
  return ajax({ url: API_RBAC_ADDFOLDER, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 更新文件夹
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function updateFolderApi(params) {
  return ajax({ url: API_RBAC_UPDATEFOLDER, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 删除文件夹
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function removeFolderApi(params) {
  return ajax({ url: API_RBAC_REMOVEFOLDER, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 添加角色
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addRoleApi(params) {
  return ajax({ url: API_RBAC_ADDROLE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 更新角色
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function upDateRoleApi(params) {
  return ajax({ url: API_RBAC_UPDATEROLE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 删除角色
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function removeRoleApi(params) {
  return ajax({ url: API_RBAC_REMOVEROLE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 移动角色
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function moveRoleApi(params) {
  return ajax({ url: API_RBAC_MOVEROLE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 加载顶级菜单
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadTopMenusApi(params, type) {
  debugger;
  let url = API_RBAC_LOADTOPMENUS;
  // 三员管理判断
  if (type == '/threeMemberManagement') {
    url = API_THEREEMEMBERMANAGEMENT_LOAD_TOP_MENUS;
  }
  return ajax({ method: 'GET', url, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 导入功能授权
 * strData:"[{id:'a',code:'b',name:'c'}]"
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function importFuncsApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_IMPORTFUNCS, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 加载功能授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadimportFuncsApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_LOADIMPORTFUNCS, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 删除功能授权
 * funcIds:''
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function removeFuncsApi(params) {
  return ajax({ url: API_RBAC_REMOVEFUNCS, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 查询全部机构
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadOrgApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_LOADORG, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 加载账户
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadAccountApi(params, type) {
  debugger;
  let url = API_RBAC_LOADACCOUNT;
  // 三员管理判断
  if (type == '/threeMemberManagement') {
    url = API_THEREEMEMBERMANAGEMENT_LOAD_ACCOUNT;
  }
  return ajax({ method: 'GET', url, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 加载账户授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadAccountPermissionTreeApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_LOADACCOUNTPERMISSIONTREE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 自主控制角色权限
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function selfControlPermByRoleApi(params) {
  return ajax({ url: API_RBAC_SELFCONTROLPERMROLE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 自主控制部门权限
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function selfControlPermByDepartment(params) {
  return ajax({ url: API_RBAC_SELFCONTROLPERMDEPARTMENT, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 取消角色自主控制
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function cancelSelfControlPermByRoleApi(params) {
  return ajax({ url: API_RBAC_CANCELSELFCONTROLPERMROLE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 自主控制部门权限
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function cancelControlPermByDepartment(params) {
  return ajax({ url: API_RBAC_CANCELSELFCONTROLPERMDEPARTMENT, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 按人员导入账号
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function importByDepartmentUsersApi(params) {
  return ajax({ url: API_RBAC_IMPORTBYDEPARTUSERS, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 按部门导入
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function importUserByDepartmentApi(params) {
  return ajax({ url: API_RBAC_IMPORTBYDEPARTMENT, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 按机构导入
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function importAccountByOrgsApi(params) {
  return ajax({ url: API_RBAC_IMPORTBYORG, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 移除账号角色
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function removeAccountApi(params) {
  return ajax({ url: API_RBAC_REMOVEACCOUNT, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 加载权限树
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadPermissionTreeApi(params, type) {
  let url = API_RBAC_LOADPERMISSIONTREE;
  // 三员管理判断
  if (type == '/threeMemberManagement') {
    url = API_THEREEMEMBERMANAGEMENT_LOAD_PERMISSION_TREE;
  }
  return ajax({ method: 'GET', url, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 加载功能授权树
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadFuncPermissionTreeApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_LOADFUNCPERMISSIONTREE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取部门树
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadDepartmentTreeApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_LOADDEPARTMENTTREE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 加载公司部门人员树
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadDepartmentTreeUserApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_LOADDEPARTMENTUSERTREE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 加载部门人员
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadRoleUserApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_LOADROLEUSER, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 保存角色菜单授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveRoleMenuPermissionApi(params, type) {
  let url = API_RBAC_SAVEROLEMENUPERMISSION;
  // 三员管理判断
  if (type == '/threeMemberManagement') {
    url = API_THEREEMEMBERMANAGEMENT_SAVE_POLE_MENU_PERMISSION;
  }
  return ajax({ url: API_RBAC_SAVEROLEMENUPERMISSION, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 保存部门菜单授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveDeptMenuPermissionApi(params) {
  return ajax({ url: API_RBAC_SAVEDEPMENUPERMISSION, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 保存机构菜单授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveOrgMenuPermissionApi(params) {
  return ajax({ url: API_RBAC_SAVEORGMENUPERMISSION, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 保存权限组菜单授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function savePermGroupMenuPermissionApi(params) {
  return ajax({ url: API_RBAC_SAVEPERMGROUPMENUPERMISSION, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 保存开发组菜单授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveDevelopMenuPermissionApi(params) {
  return ajax({ url: API_RBAC_SAVEDEVELOPMENUPERMISSION, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 保存角色功能授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveRoleFuncPermissionApi(params) {
  return ajax({ url: API_RBAC_SAVEROLEFUNCPERMISSION, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 保存部门功能授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveDeptFuncPermissionApi(params) {
  return ajax({ url: API_RBAC_SAVEDEPFUNCPERMISSION, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 保存机构功能授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveOrgFuncPermissionApi(params) {
  return ajax({ url: API_RBAC_SAVEORGFUNCPERMISSION, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 保存权限组功能授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function savePermGroupFuncPermissionApi(params) {
  return ajax({ url: API_RBAC_SAVEPERMGROUPFUNCPERMISSION, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 保存开发组功能授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveDevelopFuncPermissionApi(params) {
  return ajax({ url: API_RBAC_SAVEDEVELOPFUNCPERMISSION, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 移除部门人员账号
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function removeDeptUserApi(params) {
  return ajax({ url: API_RBAC_REMOVEDEPTUSER, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 移除所有部门人员账号
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function removeAllDeptUserApi(params) {
  return ajax({ url: API_RBAC_REMOVEALLDEPTUSER, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 加载权限组
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadPermGroupApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_LOADPERMGROUPS, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 创建权限组
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function savePermGroupApi(params) {
  return ajax({ url: API_RBAC_SAVEPERMGROUPS, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 移除权限组
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function removePermGroupApi(params) {
  return ajax({ url: API_RBAC_REMOVEPERMGROUPS, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 更新权限组
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function updatePermGroupApi(params) {
  return ajax({ url: API_RBAC_UPDATEPERMGROUPS, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 移除权限组
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function moveGroupApi(params) {
  return ajax({ url: API_RBAC_MOVEGROUPS, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 加载权限组
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadDevRoleTreeApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_DEVELOPLOADROLETREE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 开发组-添加文件夹
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addDevFolderApi(params) {
  return ajax({ url: API_RBAC_ADDDEVFOLDER, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 开发组-更新文件夹
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function updateDevFolderApi(params) {
  return ajax({ url: API_RBAC_UPDATEDEVFOLDER, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 开发组-删除文件夹
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function removeDevFolderApi(params) {
  return ajax({ url: API_RBAC_REMOVEDEVFOLDER, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 开发组-添加角色
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addDevRoleApi(params) {
  return ajax({ url: API_RBAC_ADDDEVROLE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 开发组-更新角色
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function upDateDevRoleApi(params) {
  return ajax({ url: API_RBAC_UPDATEDEVROLE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 开发组-删除角色
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function removeDevRoleApi(params) {
  return ajax({ url: API_RBAC_REMOVEDEVROLE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 开发组-移动角色
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function moveDevRoleApi(params) {
  return ajax({ url: API_RBAC_MOVEDEVROLE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 开发者显隐
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadDevelopTabApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_LOADDEVELOPTAB, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 开发者删除账号
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function removeDevelopAccountDeptRoleApi(params) {
  return ajax({ url: REMOVEDEVELOPACCOUNTDEPTROLE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 开发者导入账号
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function importAccountDeptByDepartmentAccounteApi(params) {
  return ajax({ url: API_RBAC_IMPOTRACCOUNTDDEPTBYDDEPTACCOUNT, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * SAAS角色权限 查看规划产品角色树
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function findProductRoleTree(params) {
  return ajax({ url: API_SAAS_RBAC_FINDPRODUCTROLETREE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * SAAS角色权限 查看规划产品顶级菜单
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadTopMenus(params) {
  return ajax({ url: API_SAAS_RBAC_LOADTOPMENUS, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * SAAS角色权限 查看规划产品顶级功能
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadTopFuncs(params) {
  return ajax({ url: API_SAAS_RBAC_LOADTOPFUNCS, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * SAAS角色权限 查看规划产品菜单树
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function findProductMenuTree(params) {
  return ajax({ url: API_SAAS_RBAC_FINDPRODUCTMENUTREE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * SAAS角色权限 查看规划产品功能树
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function findProductFuncTree(params) {
  return ajax({ url: API_SAAS_RBAC_FINDPRODUCTFUNCTREE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * SAAS角色权限 规划产品授权
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doGrant(params) {
  return ajax({ url: API_SAAS_RBAC_GRANT, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

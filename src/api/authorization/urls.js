/**
 * 数据权限相关接口
 * API_URL命名规则：API_模块_方法
 */

// 得到表单树
export const API_AUTHORIZATION_LOADGRIDTREEDATA = '/je/meta/funcInfo/loadGridTreeData';

// 获取数据
export const API_AUTHORIZATION_GETPERMEDATA = '/je/meta/funcPerm/getPermData';

// 权限保存
export const API_AUTHORIZATION_DOSAVE = '/je/meta/funcPerm/doSavePerm';

// 加载角色树
export const API_RBAC_LOADROLETREE = '/je/rbac/cloud/grant/role/loadRoleTree';

// 获取部门树
export const API_RBAC_LOADDEPARTMENTTREE = '/je/rbac/cloud/grant/department/loadDepartmentTree';

// 查询全部机构
export const API_RBAC_LOADORG = '/je/rbac/cloud/grant/org/loadOrgs';

// 获取功能用到的字典
export const API_AUTHORIZATION_GETFUNCFIELDDIC = '/je/meta/funcPerm/getFuncFieldDic';

// 获取功能列表数据
export const API_AUTHORIZATION_LOADFUNC = '/je/meta/funcInfo/loadFunc';

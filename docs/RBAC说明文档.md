# 说明文档

```bash
src
│   app.vue                               #根组件
│   main.js                               #JS入口文件
│
├───api                                   #角色权限：接口文件
│   │   index.js                          #角色权限：用于编写api方法
│   │   urls.js                           #角色权限：接口地址文档
│   │
│   └───authorization                     #数据权限：接口文件
│           index.js                      #数据权限：用于编写api方法
│           urls.js                       #数据权限：接口地址文档
│
├───data                                  #数据
│       table.js                          #table的模拟数据
│       tree.js                           #tree的模拟数据
│
├───helper                                #公共数据
│   │   constant.js                       #公共变量
│   │
│   └───model                             #公共模块
│           event.js                      #vueEvent方法
│
├───hooks                                 #hooks钩子
│       use-data-authority.js             #数据权限弹窗方法
│       use-dialog.js                     #确认保存，确认离开，取消三选项提示框调用方法
│
├───locales                               #国际化
│   │   index.js                          #国际化入口文件
│   │
│   └───lang                              #语言
│           en_US.js                      #英文
│           zh_CN.js                      #中文
│
├───router                                #路由
│       index.js                          #路由配置文件
│
├───store                                 #公共状态
│       app-store.js                      #公共状态管理文件
│
├───utils                                 #工具
│       validate.js                       #表单校验方法
│
└───views                                 #UI页面
    │   index.vue                         #UI页面入口文件
    │
    ├───data-authority                    #数据权限模块
    │   │   index.vue                     #数据权限组件
    │   │
    │   └───open-dialog                   #数据权限弹框
    │       │   index.vue                 #数据权限弹框组件
    │       │
    │       ├───authorization-field       #字段授权
    │       │       index.vue             #字段授权组件
    │       │
    │       ├───control-field             #控制字段
    │       │       index.vue             #控制字段组件
    │       │
    │       ├───dictionary-authorizatio   #字典授权
    │       │       index.vue             #字典授权组件
    │       │
    │       ├───rapid-authorization       #快速授权
    │       │       index.vue             #快速授权组件
    │       │       list-litem.vue        #复选按钮列表组件
    │       │       tree-select-item.vue  #tree选取器筛选组件
    │       │
    │       ├───role-sql-authorization    #角色SQL授权
    │       │       index.vue             #角色SQL授权组件
    │       │
    │       └───sql-authorization         #SQL授权
    │               index.vue             #SQL授权组件
    │
    └───role-authorization                #角色权限模块
        │   index.vue                     #角色权限组件
        │
        ├───account                       #账户配置
        │       import-by-department.vue  #按部门导入组件
        │       index.vue                 #账户配置组件
        │       view-permission.vue       #查看权限组件
        │
        ├───department                    #组织
        │       index.vue                 #组织组件
        │
        ├───developer                     #开发者
        │       add-edit-role.vue         #开发者：添加编辑角色组件
        │       add-file.vue              #开发者：添加分类组件
        │       index.vue                 #开发者组件
        │       transfer.vue              #开发者：转移组件
        │
        ├───organization                  #机构
        │       index.vue                 #机构组件
        │
        ├───permission                    #权限配置
        │       index.vue                 #权限配置组件
        │
        ├───permission-group              #权限组
        │       add-permission.vue        #添加权限组组件
        │       index.vue                 #权限组组件
        │
        └───role                          #角色
        │       add-edit-role.vue         #角色：添加编辑角色组件
        │       add-file.vue              #角色：添加分类组件
        │       index.vue                 #角色组件
        │       transfer.vue              #角色：转移组件
        
```